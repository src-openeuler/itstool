Name:           itstool
Version:        2.0.7
Release:        1
Summary:        ITS-based XML translation tool
License:        GPLv3+
URL:            http://itstool.org/
Source0:        http://files.itstool.org/itstool/%{name}-%{version}.tar.bz2
BuildArch:      noarch

BuildRequires:  python3-libxml2 python3-devel 
Requires:       python3-libxml2

%description
ITS Tool allows you to translate your XML documents with PO files, using rules from the W3C Internationalization Tag Set (ITS)\
to determine what to translate and how to separate it into PO file messages

%package        help
Summary:        Help manual for %{name}

%description    help
The %{name}-help package conatins man manual etc

%prep
%autosetup -n %{name}-%{version} -p1

%build
export PYTHON=%{__python3}
%configure
%make_build

%install
%make_install

%files
%license COPYING COPYING.GPL3
%{_bindir}/itstool
%{_datadir}/itstool

%files help
%doc NEWS
%doc %{_mandir}/man1/itstool.1.gz

%changelog
* Wed Jun 15 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.0.7-1
- Upgrade to version 2.0.7

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.0.6-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.6-1
- Update to 2.0.6

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.4-5
- Package init
